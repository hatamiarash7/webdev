# WebDev - Best Dockerized Playground 
For those who wants to enjoy and develop php application this is the best solution!

### Services
- HTTP Apache
- PHP 7.2
- MySQL 5.7
- phpMyAdmin
- MongoDB
- Redis
- PostgreSQL
- RabbitMQ
- Elasticsearch + Kibana

### PHP Modules
bcmath, bz2, calendar, Core, ctype, curl, date, dom, exif, fileinfo, filter, ftp, gd, hash, iconv, igbinary, intl, json, libxml, mbstring, mongodb, mysqlnd, openssl, pcre, PDO, pdo_mysql, pdo_pgsql, pdo_sqlite, Phar, posix, readline, redis, Reflection, session, SimpleXML, sockets, sodium, SPL, sqlite3, standard, tokenizer, xml, xmlreader, xmlwriter, Zend OPcache, zip, zlib

### Privileges
- Sudo
- None Sudo

### Requirements
- Docker
- Docker Compose
- Git
- Bash

### Installation
Add ```127.0.0.1 hello.local``` to ```/etc/hosts``` file and then run the below commands
```
git clone git@gitlab.com:mrcyna/webdev.git
cd webdev
chmod +x webdev
./webdev
```

### How to Add New Site
Just edit the ```containers/http/000-default.conf``` and add your vHost directive like below:
```
<VirtualHost *:8001>
    ServerName www.foo.local
    ServerAlias foo.local
    DocumentRoot /var/www/html/foo
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```
Create the folder structure ```www/foo/index.php```

Add ```127.0.0.1 foo.local``` to ```/etc/hosts```

And Run ```./webdev restart```

Congratulation! Now you can browse your site => http://foo.local/ 


### Dashboards
- Apache => http://hello.local/ phpinfo()
- phpMyAdmin => http://localhost:8888/
- RabbitMQ => http://localhost:15672/
- Kibana => http://localhost:5601/

### Environment Variables
##### MySQL
    DB_CONNECTION=mysql
    DB_HOST=mysql
    DB_PORT=3306
    DB_DATABASE=laravel
    DB_USERNAME=root
    DB_PASSWORD=root

##### MongoDB
    MONGODB_CONNECTION=mongodb
    MONGODB_HOST=mongodb
    MONGODB_PORT=27017
    MONGODB_DATABASE=laravel
    MONGODB_USERNAME=root
    MONGODB_PASSWORD=root

##### PostgreSQL
    PGDB_CONNECTION=pgsql
    PGDB_HOST=postgres
    PGDB_PORT=5432
    PGDB_DATABASE=laravel
    PGDB_USERNAME=postgres
    PGDB_PASSWORD=root

##### Redis
    REDIS_HOST=redis
    REDIS_PASSWORD=null
    REDIS_PORT=6379

##### RabbitMQ
    AMQP_HOST=rabbitmq
    AMQP_PORT=5672
    AMQP_USERNAME=guest
    AMQP_PASSWORD=guest
    AMQP_VHOST=/

##### Elasticsearch
    ELASTICSEARCH=http://elasticsearch:9200/